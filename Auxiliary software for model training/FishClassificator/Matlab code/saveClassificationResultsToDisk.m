function saveClassificationResultsToDisk(imageStore, pred, scores, classNames, tableFile, imageFolder)
% Save results to table
fprintf("Saving prediction table at: '%s'... ", tableFile);
imageFiles = imageStore.Files;
path = fileparts(tableFile);
if ~isfolder(path)
    mkdir(path);
end
t = table(imageFiles, pred, 'VariableNames', {'Images', 'Prediction'});
writetable(t, tableFile, "Sheet", "Prediction");
t = [table(imageFiles, 'VariableNames', {'Images'}), array2table(scores, 'VariableNames', classNames)];
writetable(t, tableFile, "Sheet", "Scores");

if nargin > 5
    fprintf("Copying images to: '%s'... \n", imageFolder);
    
    imagePaths = cellfun(@fileparts, imageFiles, 'UniformOutput', false);
    filenameStartIndex = find(any(diff(char(imagePaths(:))),1),1,'first');
    if isempty(filenameStartIndex)
        filenameStartIndex = numel(imagePaths{1}) + 1;
    end
    
    for idx = 1:numel(imageFiles)
        file = imageFolder;
        if ~isempty(imageStore.Labels)
            if imageStore.Labels(idx) == pred(idx)
                file = fullfile(file, "Correct");
            else
                file = fullfile(file, "Not Correct");
            end
        end
        file = fullfile(file, string(pred(idx)));
        sourceFile = imageFiles{idx};
        file = fullfile(file, sourceFile(filenameStartIndex:end));
        [path, ~,~] = fileparts(file);
        if ~isfolder(path)
            mkdir(path);
        end
        fprintf("\tCopying image from '%s' to '%s'... ", sourceFile, file);
        [status, errMsg] = copyfile(sourceFile, file);
        if status == 1
            fprintf("Success\n");
        else
            fprintf("Error: %s\n", errMsg);
        end
    end
end
end