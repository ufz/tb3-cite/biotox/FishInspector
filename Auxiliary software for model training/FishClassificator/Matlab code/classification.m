%% Parameters

% Path to model file
modelFilename = "Z:\FishClassificator\Orientation_Model.mat";
useFoldernamesAsLabels = false;

% Path to the image files
% This can either be a directory or a tabular data file
% If it is a directory, labels for validation are read from the subfolder name
% If the images are not stored in subdirectories, there is no validation
% If it is a file, the images (& labels for validation, if there are any) are read from their respective column
path = "Z:\FishClassificator\Leica images with position information for classification training.xls";
imageColumnName = "Filename";
labelColumnName = "Position";

% Result directory
resultsPath = "Z:\FishClassificator\Orientation";
% Toggles if images are copied into respective label folders or if only the results table is saved
copyImagesIntoLabelFolder = true;


%% Classification

% Load image data
if isfolder(path)
    % If the given path is a directory, the folder names are used as class labels
    if useFoldernamesAsLabels
        fprintf("Loading images & labels to classify from directory: '%s'... ", path);
        labelSource = "foldernames";
    else
        fprintf("Loading images to classify from directory: '%s'... ", path);
        labelSource = "none";
    end
    imageStore = imageDatastore(path, "IncludeSubfolders", true, "LabelSource", labelSource);
elseif isfile(path)
    fprintf("Loading images to classify from table file: '%s'... ", path);
    % If the given path is a file path, read file as table (e.g. excel)
    % The first column must contain the image file locations, and the second column the class labels
    t = readtable(path);
    files = string(t.(imageColumnName));
    imageStore = imageDatastore(files);
    % Add labels if available
    if labelColumnName ~= ""
        imageStore.Labels = categorical(t.(labelColumnName));
    end
else
    throw(MException("PathNotRecognized", "Path not valid"));
end
fprintf("Found %d images\n", numel(imageStore.Files));


% Load model
fprintf("Loading neural network from file: '%s'...\n", modelFilename);
mat = matfile(modelFilename);
net = mat.net;
imgSize = mat.imgSize;
outputLayer = net.Layers({net.Layers.Name} == string(net.OutputNames{1}));
classNames = string(outputLayer.Classes);

% Classify images
fprintf("Classifing images... ");
augmentedImages = augmentedImageDatastore(imgSize, imageStore, "ColorPreprocessing", "gray2rgb");
[pred,scores] = classify(net,augmentedImages);
if ~isempty(imageStore.Labels)
    accuracy = mean(imageStore.Labels == pred);
    fprintf("The network accuracy is %.2f%%", accuracy*100);
end
fprintf("\n");

tableFile = fullfile(resultsPath, "predictions.xls");
if copyImagesIntoLabelFolder
    saveClassificationResultsToDisk(imageStore, pred, scrores, classNames, tableFile, resultsPath);
else
    saveClassificationResultsToDisk(imageStore, pred, scrores, classNames, tableFile);
end

fprintf("Finished!\n");